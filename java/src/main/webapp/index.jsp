<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Form</h1>
        <form action="MyServlet" method="post">

            First name: <input type="text" name="firstname" />
            <br />
            <br />Last name: <input type="text" name="lastname" />
            <br />
            <br />Phone number: <input type="text" name="phonenumber" />
            <br />
            <br />Email: <input type="text" name="email" />
            <br />
            <br />Adress: <input type="text" name="adress" />
            <br />
            <br />City: <input type="text" name="city" />
            <br />
            <br />Zip code: <input type="text" name="zip" />
            <br />
            <br />Is friend?: <input type="checkbox" name="isfriend" />
            <br />
            <br /><input type="submit" value="Save" />

        </form>
    </body>
</html>
