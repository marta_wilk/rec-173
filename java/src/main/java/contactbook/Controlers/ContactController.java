package contactbook.Controlers;

import contactbook.Model.Contact;
import java.util.List; 
import java.util.Date;
import java.util.Iterator; 
 
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ContactController {
   private static SessionFactory factory; 
   public static void main(String[] args) {
      try{
         factory = new Configuration().configure().buildSessionFactory();
      }catch (Throwable ex) { 
         System.err.println("Failed to create sessionFactory object." + ex);
         throw new ExceptionInInitializerError(ex); 
      }
     
   }
   
   /**
    * Method to add contact to database.
    * 
    * @param fname
    * @param lname
    * @param salary
    * @return 
    */
   public Integer addContact(String firstname, String lastname, String phonenumber, 
                                String email, String address, String city, String zip, String idfriend){
      Session session = factory.openSession();
      Transaction tx = null;
      Integer contactID = null;
      try{
         tx = session.beginTransaction();
         Contact contact = new Contact();
         contactID = (Integer) session.save(contact); 
         tx.commit();
      }catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      }finally {
         session.close(); 
      }
      return contactID;
   }
   
   /**
    * Method to  READ all the contacts
    */
   public void listContacts( ){
      Session session = factory.openSession();
      Transaction tx = null;
      try{
         tx = session.beginTransaction();
         List contacts = session.createQuery("FROM Employee").list(); 
         for (Iterator iterator = 
                           contacts.iterator(); iterator.hasNext();){
            Contact contact = (Contact) iterator.next(); 
            System.out.print("Name: " + contact.getFirstname() + " " + contact.getLastname()); 
            System.out.println("Phone Number: " + contact.getPhonenumber()); 
            System.out.println("E-mail: " + contact.getEmail()); 
            System.out.println("Address: " + contact.getAddress() + ", " + contact.getZip() 
                    + " " + contact.getCity());  
         }
         tx.commit();
      }catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      }finally {
         session.close(); 
      }
   }
   
   /**
    * Method to UPDATE phonenumber contact
    * 
    * @param EmployeeID
    * @param salary 
    */
   public void updateContact(Integer ContactID, String phonenumber ){
      Session session = factory.openSession();
      Transaction tx = null;
      try{
         tx = session.beginTransaction();
         Contact contact = 
                    (Contact)session.get(Contact.class, ContactID); 
         contact.setPhonenumber( phonenumber );
		 session.update(contact); 
         tx.commit();
      }catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      }finally {
         session.close(); 
      }
   }
   
   /**
    * Method to DELETE an contact from the records
    * 
    * @param EmployeeID 
    */
   public void deleteContact(Integer ContactID){
      Session session = factory.openSession();
      Transaction tx = null;
      try{
         tx = session.beginTransaction();
         Contact employee = 
                   (Contact)session.get(Contact.class, ContactID); 
         session.delete(employee); 
         tx.commit();
      }catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      }finally {
         session.close(); 
      }
   }
}