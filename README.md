# Documentation: **/Angular** #

* **Quick summary:** Application could you build your customized form. Work in progress.

* **Version:** 1.0 (beta)

* ** Instalation Application:** Clone repository and use.

# Documentation: **/PHP** #

* **Quick summary:** ContactsBook is application, where users can add contacts by form. 

* **Version:** 1.0 (beta)

* **Information in ContactsBook:** firstname, lastname, address, zip, city, phonnumber

* ** Instalation ContactsBook Application:**
 1. Clone project from repository
 2. Build project Symfon2 using composer | command: composer install
 3. Add this project to your IDE f.e. Netbeans
 4. Upload database from 'db' catalog 

# Documentation: **/Java** #

* **Quick summary:** ContactsBook is application, where users can add contacts by form.

* **Version:** 1.0 (beta)

* ** Instalation ContactsBook Application:** Clone repository or download, add to your Java IDE -> Maven -> Existing project -> next please Build Depedencies.


* ** Team contact:** If you have any questions: Jacek Koprowski* email: <jacek@kopering.net>
 phone: +48 697 324 354