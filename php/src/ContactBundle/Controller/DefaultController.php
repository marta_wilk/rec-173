<?php

namespace ContactBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ContactBundle\Form\Type\BookType;
use ContactBundle\Form\Type\ContactType;
use ContactBundle\Form\Model\Book;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    
    /**
     * Method show all contacts from ContactsBook
     * 
     * @return type
     */
    public function showAction()
    {
        $em = $this->getDoctrine()->getManager();
        $contacts = $em->getRepository('ContactBundle:Contact')->findBy(array(), 
                array('lastname' => 'ASC'));

        return $this->render(
            'ContactBundle:Default:show.html.twig',
            array('contacts' => $contacts)
        );
    }


    /**
     * Method add form to view.
     * 
     * @return type
     */
    public function addAction()
    {
        $book = new Book();
        $form = $this->createForm(new BookType(), $book, array(
            'action' => $this->generateUrl('contact_create'),
        ));

        return $this->render(
            'ContactBundle:Default:add.html.twig',
            array('form' => $form->createView())
        );
    }
    
    /**
     * Method add contact to Database MySQL
     * 
     * @param Request $request
     * @return type
     */
    public function createAction(Request $request)
    {
    
    $em = $this->getDoctrine()->getManager();
    $form = $this->createForm(new BookType(), new Book());
    $form->handleRequest($request);

    if ($form->isValid()) {
        $book = $form->getData();

        $em->persist($book->getContact());
        $em->flush();

        return $this->redirectToRoute('contact_show');
    }


        return $this->render(
            'ContactBundle:Default:add.html.twig',
            array('form' => $form->createView())
        );
    }
    
    /**
     * Update data in Database.
     * 
     * @param type $id
     * @return type
     */
    public function updateAction(Request $request, $id) {
        
        $em = $this->getDoctrine()->getManager();

        $contacts = $em->getRepository('ContactBundle:Contact')->findById($id);

        if (count($contacts) == 0) {
            $this->addFlash('error', 'Contact not exist!');
            return $this->redirect($this->generateUrl('add_contact'));
        }
        
        $contact = $contacts[0];
        $form = $this->createForm(new ContactType(), $contact);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();

            $this->addFlash('success', 'Contact edited!');
            
            return $this->redirect($this->generateUrl('contact_show'));
        }

        return $this->render(
            'ContactBundle:Default:update.html.twig',
            array('form' => $form->createView())
        );
    }
    
    /**
     * Action to delete one record from Database
     * 
     * @param type $id
     * @return type
     */
    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $contacts = $em->getRepository('ContactBundle:Contact')->findById($id);

        if (count($contacts) == 0) {
            $this->addFlash('error', 'Contact not exist!');
            return $this->redirect($this->generateUrl('contact_show'));
        }

        $contact = $contacts[0];

        $em->remove($contact);
        $em->flush();

        $this->addFlash('success', 'Contact deleted!');
        return $this->redirect($this->generateUrl('add_contact'));
    }

    /**
     * Method clear all data from ContactBooks
     * 
     * @param Request $request
     * @return type
     */
    public function clearAction(Request $request) {
        $idi = $request->query->get('id');
        $ids = explode('_', $idi);

        $em = $this->getDoctrine()->getManager();

        foreach ($ids as $id) {
            $contacts = $em->getRepository('ContactBundle:Contact')->findById($id);

            if (count($contacts) <> 0) {
                $contact = $contacts[0];
                $em->remove($contact);
                $em->flush();
            }
        }

        $this->addFlash('success', 'Database is clear!');
        return $this->redirect($this->generateUrl('contact_show'));
    }

}
