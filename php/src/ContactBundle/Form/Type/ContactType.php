<?php

namespace ContactBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType {
    
    /**
     * Method to build form.
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstname', 'text')
                ->add('lastname', 'text')
                ->add('phonenumber', 'text')
                ->add('email', 'email')
                ->add('address', 'textarea')
                ->add('city', 'text')
                ->add('zip', 'text')
                ->add('isfriends', 'checkbox');
       
    }
    
    /**
     * 
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ContactBundle\Entity\Contact'
        ));
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {
        return 'contact';
    }
}