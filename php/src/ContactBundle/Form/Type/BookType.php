<?php

namespace ContactBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class BookType extends AbstractType {
    
    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('contact', new ContactType());
        $builder->add('Add Contact', 'submit');
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {
        return 'addcontact';    
    }
}
