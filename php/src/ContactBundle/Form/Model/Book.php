<?php

namespace ContactBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

use ContactBundle\Entity\Contact;

class Book {
 
    /**
     * @Assert\Type(type="ContactBundle\Entity\Contact")
     * @Assert\Valid()
     */
    protected $contact;
    
    function getContact() {
        return $this->contact;
    }

    function setContact($contact) {
        $this->contact = $contact;
    }

}
