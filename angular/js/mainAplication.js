var app = angular.module('dragDrop', []);

app.controller('DragDropCtrl', function ($scope, $attrs) {
    $scope.handleDrop = function () {
        alert('Choosen item has been dropped to your form.');
        
    };
});

app.directive('draggable', function () {
    return function (scope, element) {
        
        var el = element[0];

        el.draggable = true;

        el.addEventListener(
                'dragstart',
                function (e) {
                    e.dataTransfer.effectAllowed = 'move';
                    e.dataTransfer.setData('Text', this.id);
                    this.classList.add('drag');
                    return false;
                },
                false
                );

        el.addEventListener(
                'dragend',
                function (e) {
                    this.classList.remove('drag');
                    return false;
                },
                false
                );
    }
});

app.directive('droppable', function () {
    return {
        scope: {
            drop: '&',
            bin: '=' // bi-directional scope
        },
        link: function (scope, element) {
            // again we need the native object
            var el = element[0];

            el.addEventListener(
                    'drop',
                    function (e) {
                        // Stops some browsers from redirecting.
                        if (e.stopPropagation)
                            e.stopPropagation();

                        if (e.preventDefault) {
                            e.preventDefault();
                        }
                    
                    this.classList.remove('over');
                    var item = document.getElementById(e.dataTransfer.getData('Text'));
                    this.appendChild(item.cloneNode(true));
                        // call the drop passed drop function
                        
                        scope.$apply('drop()');

                        return false;
                    },
                    false
                    );
            el.addEventListener(
                    'dragover',
                    function (e) {
                        e.dataTransfer.dropEffect = 'move';
                        // allows us to drop
                        if (e.preventDefault)
                            e.preventDefault();
                        this.classList.add('over');
                        return false;
                    },
                    false
                    );
            el.addEventListener(
                    'dragenter',
                    function (e) {
                        this.classList.add('over');
                        return false;
                    },
                    false
                    );

            el.addEventListener(
                    'dragleave',
                    function (e) {
                        this.classList.remove('over');
                        return false;
                    },
                    false
                    );

                    
        }
    }

});



