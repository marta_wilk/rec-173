/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* $http ajax calls really belongs in a service, 
but I'll be using them inside the controller for this demo */ 

app.controller('jsonController', function($scope, $http) {
  $http.get('../data/formData.json').then(function(data) {
    $scope.forms = data;
  });
  //inputting json directly for this example
  
  $scope.save = function() {
    $http.post('../data/formData.json', $scope.forms).then(function(data) {
      $scope.msg = 'Data saved';
    });
    $scope.msg = 'Data sent: '+ JSON.stringify($scope.forms);
  };
});